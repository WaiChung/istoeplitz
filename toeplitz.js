// lessons about for loop
// 1) code that is run before the loop happens
//2) constraints that loop is gonna run under
// 3) code that is to run after the loop happens 
var matrix = [[6,7,8,9,2],
			 [4,6,7,8,9],
			 [1,4,6,7,8],
			 [0,1,4,6,7]
			 ];

function IsToepliz(oMtx,nRows,nColumns) {
  var nConst;
  for(var i=0; i<nColumns; i++)
    for(var j=0, nConst=oMtx[0][i]; j<nRows && j+i<nColumns; j++)
    	console.log(nConst);
       if(oMtx[j][j+i] != nConst)
        return false;
		
  for(var i=1; i<nRows; i++)
    for(var j=0, nConst=oMtx[i][0]; j<nColumns && j+i<nRows; j++)
      if(oMtx[i+j][j] != nConst)
        return false;
		
  return true;
}	
IsToepliz(matrix, 4, 5);